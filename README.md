## Notes
Find something along the lines of:
`xxx.default-release`

In:
`\AppData\Roaming\Mozilla\Firefox\Profiles`


Place userChrome.css (or what's in symlink-live, or other) in:
`\AppData\Roaming\Mozilla\Firefox\Profiles\xxx.default-release\chrome`
    
[Image of my profile folder](https://imgur.com/BsEnu0f)
    
The chrome folder won't exist; that's how it is by default, so just create it
or create a symbolic link of my folder into the profile and rename it to chrome

[Image of the contents](https://imgur.com/bqGt3x0)



## Tree Style Tab
Open renamable blank "folder" tab (paste into URL): `about:treestyletab-group?title=New%20group&temporary=false`
